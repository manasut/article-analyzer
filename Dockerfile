FROM python:3.7-slim

# Python packages
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install numpy
RUN pip install -r requirements.txt
COPY . .
CMD ["python", "main.py"]
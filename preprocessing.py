# all rights to https://github.com/suraj-deshmukh/BBC-Dataset-News-Classification/blob/master/model/get_data.py
import os
import pandas as pd

data_folder = "./dataset/"
folders = ["business", "entertainment", "politics", "sport", "tech"]
types = {
    "business": 2,
    "politics": 0,
    "tech": 4,
    "sport": 1,
    "entertainment": 3,
}
reverse_types = {
    2: "business",
    0: "politics",
    4: "tech",
    1: "sport",
    3: "entertainment",
}
if __name__ == "__main__":
    os.chdir(data_folder)

    title = []
    content = []
    category = []
    category_id = []
    filename = []


    for i in folders:
        files = os.listdir(i)
        for text_file in files:
            file_path = i + "/" + text_file
            print("reading file:", file_path)
            with open(file_path, encoding="ISO-8859-1") as f:
                try:
                    data = f.readlines()
                except Exception as err:
                    print(err)
                    print(file_path)
                    raise err
            t = data[0]
            body = ' '.join(data[1:])
            cate = i
            cate_id = types[cate]
            title.append(t)
            content.append(body)
            category.append(cate)
            category_id.append(cate_id)
            filename.append(text_file)

    data = {'category': category,
            'filename': filename,
            'title': title,
            'content': content,
            'category_id': category_id}
    df = pd.DataFrame(data)
    print('writing csv flie ...')
    df.to_csv('dataset.csv', index=False)

import logging
import time
import pika
import json
from bson import ObjectId
import pymongo
import os
import base64
from textblob import TextBlob
from sklearn.feature_extraction.text import TfidfVectorizer
from preprocessing import reverse_types
import pickle
from sklearn.linear_model import LogisticRegression


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
RABBIT_PORT = os.getenv('RABBIT_PORT', '5672')
STATUS_QUEUE = os.getenv('STATUS_QUEUE', 'status')
SENTIMENT_ANALYSIS_QUEUE = os.getenv('SENTIMENT_ANALYSIS_QUEUE', 'sentiment')
MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', '27017')


class RMQ:
    def __init__(self, host, port, log_name=__name__, logger=None):
        self.host = host
        self.port = int(port)
        self.logger = logger or logging.getLogger(log_name)
        start = int(time.time() * 1000.0)
        current = int(time.time() * 1000.0)
        connected = False
        while not connected and current - start < 30000:
            try:
                current = int(time.time() * 1000.0)
                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, port=int(port)))
                connected = True
            except Exception as err:
                self.logger.error("Error: {0}".format(err))
                time.sleep(6)
        if not connected:
            raise ConnectionError

    def publish(self, msg, queue):
        try:
            if self.connection.is_closed:
                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.host,
                                                                                    port=int(self.port)))
            channel = self.connection.channel()
            channel.queue_declare(queue=queue)
            channel.basic_publish(exchange="",
                                  routing_key=queue,
                                  body=msg)
        except Exception as err:
            self.logger.error("Error: {0}".format(err))

    def listen(self, queue, callback):
        try:
            if self.connection.is_closed:
                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.host,
                                                                                    port=int(self.port)))
            channel = self.connection.channel()
            channel.queue_declare(queue=queue)
            channel.basic_consume(callback,
                                  queue=queue,
                                  no_ack=True)
            channel.start_consuming()
        except Exception as err:
            self.logger.error("Error: {0}".format(err))


# class ES:
#     def __init__(self, host, port, log_name=__name__, logger=None):
#         self.host = host
#         self.port = port
#         self.logger = logger or logging.getLogger(log_name)


class MGDB:
    def __init__(self, host, port, log_name=__name__, logger=None):
        self.host = host
        self.port = int(port)
        self.logger = logger or logging.getLogger(log_name)
        connected = False
        start = int(time.time()*1000.0)
        current = int(time.time()*1000.0)
        while not connected and current - start < 30000:
            try:
                current = int(time.time()*1000.0)
                self.client = pymongo.MongoClient(host=host, port=int(port))
                connected = True
            except Exception as err:
                self.logger.error("Error: {0}".format(err))
                time.sleep(6)
        if not connected:
            raise ConnectionError
        self.logger.debug("Successfully Initialize Mongo")


rmq = RMQ(RABBIT_HOST,
          RABBIT_PORT)
mgdb = MGDB(MONGO_HOST,
            MONGO_PORT)
file_name = 'finalized_model.sav'
transformer_name = 'transformer.sav'
loaded_model = pickle.load(open(file_name, 'rb'))
tfidf = pickle.load(open(transformer_name, 'rb'))


# return list of labels
# first evaluation function is simple polarity function checking
def evaluate(text):
    # sentiment analysis
    blob = TextBlob(text)
    polarity, subjectivity = blob.sentiment
    pol = "positive"
    sub = "subjective"
    if 0 < polarity < 0.1:
        pol = "neutral"
    elif 0 > polarity:
        pol = "negative"
    if subjectivity < 0.5:
        sub = "objective"
    # topic prediction
    logger.info("getting topic - transforming text")
    features = tfidf.transform([text])
    logger.info("getting topic - predicting")
    label = loaded_model.predict(features)
    logger.info("getting topic - reversing")
    topic = reverse_types[label[0]]
    logger.info("finished evaluation")
    return [pol, sub, topic]


# for storing , the database name = results, colleciton name = results
# for reading, the database name = articles, colleciton name = rss_url encoded then we decode it
# json format:
# {"id": string --> the actual id stored in the database
#  "collection": string --> the collection in the articles database}
def main_callback(ch, method, properties, body, eval_func=evaluate):
    try:
        # unpack item
        json_body = json.loads(body)
        logger.info("processing {}".format(json_body))
        c_name = json_body["collection"]
        a_id = ObjectId(json_body["id"])
        # actual url - store this for showing
        rss_url = base64.b32decode(c_name).decode("UTF-8")
        collection: pymongo.collection = mgdb.client["articles"][c_name]
        article = collection.find_one({"_id": a_id})
        title = article["title"]
        link = article["link"]
        body = article["article"]
        # analyse the sentiment
        logger.info("evaluating {}".format(json_body))
        labels = eval_func(body)
        # store the result and new labels and new rss
        logger.info("saving {}".format(json_body))
        result_storage = mgdb.client["results"]["results"]
        label_storage = mgdb.client["results"]["labels"]
        rss_storage = mgdb.client["results"]["rss"]
        for each in labels:
            res = label_storage.find_one({"label": each})
            if res is None:
                label_storage.insert_one({"label": each})
        if rss_storage.find_one({"rss": rss_url}) is None:
            rss_storage.insert_one({"rss": rss_url})
        result_storage.insert_one({
            "labels": labels,
            "title": title,
            "rss": rss_url,
            "url": link,
        })
        # send status to taro
        status_body = {"status": "OK", "message": link}
        rmq.publish(json.dumps(status_body), STATUS_QUEUE)
        logger.info("finish processing an {}".format(json_body))
    except Exception as err:
        logger.error(err)
        # send status to taro
        status_body = {"status": "ERROR", "message": err}
        rmq.publish(json.dumps(status_body), STATUS_QUEUE)


if __name__ == '__main__':
    rmq.listen(SENTIMENT_ANALYSIS_QUEUE, main_callback)

from main import MGDB
from bson import ObjectId
from main import RMQ
import os
import logging
import json


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
RABBIT_PORT = os.getenv('RABBIT_PORT', '5672')
STATUS_QUEUE = os.getenv('STATUS_QUEUE', 'status')
SENTIMENT_ANALYSIS_QUEUE = os.getenv('SENTIMENT_ANALYSIS_QUEUE', 'sentiment')
MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', '27017')
rmq = RMQ(RABBIT_HOST,
          RABBIT_PORT)
mgdb = MGDB(MONGO_HOST,
            MONGO_PORT)


def callback(ch, method, properties, body):
    json_body = json.loads(body)
    logger.info(json_body)
    for each in mgdb.client["results"]["results"].find():
        logger.info(each)
    for each in mgdb.client["results"]["rss"].find():
        logger.info(each)
    for each in mgdb.client["results"]["labels"].find():
        logger.info(each)
    logger.info(mgdb.client["results"]["results"].find())
    logger.info(mgdb.client["results"]["rss"].find())
    logger.info(mgdb.client["results"]["labels"].find())


# upload shit to mongo
def upload_test():
    id = ObjectId()
    collection_encoded = "NB2HI4B2F4XWMZLFMRZS4YTCMNUS4Y3PFZ2WWL3OMV3XGL3XN5ZGYZBPOJZXGLTYNVWA===="
    article1 = {"_id": id,
            "title": "wojeidioejdei",
            "link": "wedwdkeopde",
            "article":
                '''
                The special counsel investigating alleged Russian collusion in the 2016 US election will not be pursuing a jail sentence for former national security adviser Michael Flynn.
                Mr Flynn has admitted lying to the FBI.
                But in a memo, Robert Mueller said Mr Flynn had provided "substantial" details about links between the Trump election team and Russian officials.
                The heavily redacted memo hints at future revelations in a criminal probe that could trouble Donald Trump.
                President Trump has called the investigation a witch hunt and denies there was any collusion between his team and Russian officials to try to secure his election victory.
                The sentencing memo released on Tuesday advising that Mr Flynn should not be imprisoned could have further implications in the ongoing investigation, should other senior members of Mr Trump's team be called upon to co-operate with additional statements.
                Mr Flynn is due to be sentenced in the US District Court for the District of Columbia on 18 December.
                Mr Mueller's office released the memo as guidance to the judge who will sentence Mr Flynn, the only member of the Trump administration so far to plead guilty as a result of the collusion investigation.
                Mr Flynn has pleaded guilty to one count of making materially false statements to the FBI.
                He admitted that he discussed lifting US sanctions on Russia with the Russian ambassador to Washington before Mr Trump took office, and that he lied to the US vice-president about that conversation.
                It says Mr Flynn has provided assistance in multiple investigations and has also given information on co-ordination between the Russian government and the Trump campaign.
                Consequently, it says: "A sentence at the low end of the guideline range - including a sentence that does not impose a term of incarceration - is appropriate and warranted."
                The memo points to Mr Flynn's "exemplary" military and public service and that he "deserves credit for accepting responsibility in a timely fashion and substantially assisting the government".
                It had been thought details of Russian collusion might be included in the memo but the document says that, due to the "sensitive information about ongoing investigations", certain aspects have been sealed.
                An addendum to the memo on Mr Flynn's assistance is heavily redacted.
                But it does say Mr Flynn provided "first-hand information about the content and context of interactions between the transition team and Russian government officials" - clearly part of the ongoing investigation.
                And then there is this intriguing element. In one section there is the simple sentence "the defendant has provided substantial assistance in a criminal investigation" but everything below it is blacked out.
                Analysis by BBC North America reporter Anthony Zurcher
                Michael Flynn may end up not spending a day in prison for lying to the FBI about his contacts with Russian officials during the Trump presidential transition.
                Part of the reason is because of his clean record prior to this offence. The other part could cause Donald Trump and those close to him sleepless nights in the days ahead.
                In an addendum to the sentencing memo, Robert Mueller details the help Mr Flynn provided to his investigation in exchange for a lenient sentence. 
                Some of it is old news. The former national security advisor told Mr Mueller "first-hand" about his communications with Russians after Mr Trump's election - and similar communications by others in the transition team.
                Buried beneath thick black lines below this "useful information" is an entire section on further help Mr Flynn provided to Mr Mueller's collusion investigation. Even the subhead appears to be redacted.
                Then there's a mysterious "criminal investigation" to which Mr Flynn provided "substantial assistance". That portion of the memo, without a few introductory words, is entirely redacted. Who is being investigated? And what type of criminal wrongdoing is suspected?
                The Flynn document raises as many questions as it answers - and points toward explosive revelations to come.
                He had been a fervent supporter of Mr Trump during the election campaign and was appointed Mr Trump's national security adviser even though former President Barack Obama had warned against the appointment.
                It was a short tenure, ending in February 2017 after just 23 days, his departure forced by the revelation he had lied to Vice-President Mike Pence about the discussion with then-Russian ambassador Sergei Kislyak.
                Further allegations then arose over a failure to disclose payments from Russian and Turkish lobbyists that he was given for speeches and consulting work.
                The retired US Army three-star lieutenant-general had earlier led the Defence Intelligence Agency (DIA) but was fired, on the surface for comments about Islamist extremism, although insiders also pointed to his unpopular overhaul of the agency.
                Yes, although not on charges related to collusion with Russia.
                Mr Trump's former deputy campaign manager, Rick Gates, has admitted conspiracy and lying to investigators.
                Former campaign chairman Paul Manafort has been convicted for fraud, bank fraud and failing to disclose bank accounts. He has a plea bargain deal but has been accused of breaching it by lying to the FBI.
                Ex-Trump lawyer Michael Cohen has admitted he lied about a Trump property deal in Russia during the 2016 election.
                '''}
    logger.debug("article send to mongo")
    if mgdb.client["articles"][collection_encoded].find_one({"_id": id}) is None:
        mgdb.client["articles"][collection_encoded].insert_one(article1)
    logger.debug("publishing")
    rmq.publish(json.dumps({"id": str(id), "collection": collection_encoded}), SENTIMENT_ANALYSIS_QUEUE)
    logger.debug("listening")
    rmq.listen(STATUS_QUEUE, callback)


upload_test()
